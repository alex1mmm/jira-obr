package ru.matveev.alexey.tutorial.obr.api;

public interface MyPluginComponent
{
    String getName();
}