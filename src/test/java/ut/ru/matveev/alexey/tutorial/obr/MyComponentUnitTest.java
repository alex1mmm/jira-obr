package ut.ru.matveev.alexey.tutorial.obr;

import org.junit.Test;
import ru.matveev.alexey.tutorial.obr.api.MyPluginComponent;
import ru.matveev.alexey.tutorial.obr.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}